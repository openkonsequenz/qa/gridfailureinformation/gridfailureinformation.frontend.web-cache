/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { dateTimePickerValueConverter } from '@grid-failure-information-app/shared/utility';
import { DateTimeModel } from '@grid-failure-information-app/shared/models/date-time.model';
import { DateTimePickerComponent } from '@grid-failure-information-app/shared/components/date-time-picker/date-time-picker.component';
import { Directive, forwardRef, Input } from '@angular/core';
import { NGRX_FORM_VIEW_ADAPTER, FormViewAdapter, FormControlState } from 'ngrx-forms';

@Directive({
  selector: 'app-date-time-picker[ngrxFormControlState]',
  providers: [
    {
      provide: NGRX_FORM_VIEW_ADAPTER,
      useExisting: forwardRef(() => DateTimePickerViewAdapterDirective),
      multi: true,
    },
  ],
})
export class DateTimePickerViewAdapterDirective implements FormViewAdapter {
  @Input()
  public set ngrxFormControlState(state: FormControlState<any>) {
    this._dateTimePickerComponent.isValid = state.isValid;
  }
  constructor(private _dateTimePickerComponent: DateTimePickerComponent) {}
  public setViewValue(value: any): void {
    const dateTimeModel = !!value ? DateTimeModel.fromLocalString(value) : new DateTimeModel();
    this._dateTimePickerComponent.dateTime = dateTimeModel;
    this._dateTimePickerComponent.dateTimeViewValue = !!value ? dateTimePickerValueConverter.convertStateToViewValue(dateTimeModel) : value;
  }
  public setOnChangeCallback(onChangeCallback: (value: DateTimeModel) => void): void {
    this._dateTimePickerComponent.onValueChangeCallBack = (value: DateTimeModel) => {
      onChangeCallback(!!value ? dateTimePickerValueConverter.convertViewToStateValue(value) : value);
    };
  }
  public setOnTouchedCallback(markTouchedCallBack: () => void): void {
    this._dateTimePickerComponent.markTouched = markTouchedCallBack;
  }
  public setIsDisabled?(isDisabled: boolean): void {
    this._dateTimePickerComponent.isDisabled = isDisabled;
  }
}
