/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { DomSanitizer } from '@angular/platform-browser';
import { SanitizeHtmlPipe } from './sanitizeHtml.pipe';

describe('SanitizeHtmlPipe', () => {
  let pipe: SanitizeHtmlPipe = {} as any;
  let _sanitizer: DomSanitizer;

  beforeEach(() => {
    _sanitizer = {
      bypassSecurityTrustHtml: v => {
        return v;
      },
    } as any;

    pipe = new SanitizeHtmlPipe(_sanitizer);
  });

  it('should create', () => {
    expect(pipe).toBeTruthy();
  });

  it('should call method transform(v: string)', () => {
    const safeHtml = pipe.transform('test');
    expect(safeHtml).toBe('test');
  });
});
