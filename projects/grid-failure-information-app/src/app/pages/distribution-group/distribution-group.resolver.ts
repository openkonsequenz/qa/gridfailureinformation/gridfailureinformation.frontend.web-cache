/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { DistributionGroupSandbox } from '@grid-failure-information-app/app/pages/distribution-group/distribution-group.sandbox';
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

@Injectable()
export class DistributionGroupResolver implements Resolve<any> {
  constructor(private _sandbox: DistributionGroupSandbox) {}

  public resolve(): void {
    this._sandbox.loadDistributionGroups();
    this._sandbox.loadDistributionGroupTextPlaceholders();
  }
}
