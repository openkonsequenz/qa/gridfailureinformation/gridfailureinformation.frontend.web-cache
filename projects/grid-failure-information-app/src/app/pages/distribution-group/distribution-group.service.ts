/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { DistributionGroup, DistributionGroupMember, Contact, DistributionGroupTextPlaceholder } from '@grid-failure-information-app/shared/models';

@Injectable()
export class DistributionGroupService {
  static gridAdapter(response: any): Array<DistributionGroup> {
    return response.map(responseItem => new DistributionGroup(responseItem));
  }

  static itemAdapter(responseItem: any): DistributionGroup {
    return new DistributionGroup(responseItem);
  }

  static memberGridAdapter(response: any): Array<DistributionGroupMember> {
    return response.map(responseItem => new DistributionGroupMember(responseItem));
  }

  static contactsPageAdapter(response: any): Array<Contact> {
    return response.content.map(responseItem => new Contact(responseItem));
  }

  static placeholderAdapter(responseItem: any): DistributionGroupTextPlaceholder {
    return new DistributionGroupTextPlaceholder(responseItem);
  }
}
