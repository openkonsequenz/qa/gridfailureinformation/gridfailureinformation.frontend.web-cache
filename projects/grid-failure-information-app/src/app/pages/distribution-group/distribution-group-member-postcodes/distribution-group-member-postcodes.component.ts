/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { DistributionGroupSandbox, PostcodeInterface } from '@grid-failure-information-app/app/pages/distribution-group/distribution-group.sandbox';
import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { BaseList } from '@grid-failure-information-app/shared/components/base-components/base.list';
import { Subscription } from 'rxjs';
import { DISTRIBUTION_GROUP_MEMBER_POSTCODES_COLDEF } from '@grid-failure-information-app/app/pages/distribution-group/distribution-group-member-postcodes/distribution-group-member-postcodes-col-def';
import { SetFilterComponent } from '@grid-failure-information-app/shared/filters/ag-grid/set-filter/set-filter.component';
import { UtilService } from '@grid-failure-information-app/shared/utility/utility.service';
import { ofType } from '@ngrx/effects';
import * as distributionGroupActions from '@grid-failure-information-app/shared/store/actions/distribution-groups.action';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-distribution-group-member-postcodes',
  templateUrl: './distribution-group-member-postcodes.component.html',
  styleUrls: ['./distribution-group-member-postcodes.component.scss'],
})
export class DistributionGroupMemberPostcodesComponent extends BaseList implements OnInit {
  @ViewChild('postcodeInput', { static: true }) postcodeInput: ElementRef;
  public columnDefinition: any = DISTRIBUTION_GROUP_MEMBER_POSTCODES_COLDEF;
  public frameworkComponents: { setFilterComponent: any };

  @Input() set plzValueId(value: string) {
    if (!this._currentPlzValueId || this._currentPlzValueId !== value) {
      this._currentPlzValueId = value;
      this.postcodeInput.nativeElement.value = '';
    }
  }
  private _currentPlzValueId: string;

  constructor(public sandbox: DistributionGroupSandbox, private _utilService: UtilService) {
    super();
    this.frameworkComponents = { setFilterComponent: SetFilterComponent };
  }

  ngOnInit() {
    this.gridOptions.context = {
      ...this.gridOptions.context,
      icons: { delete: true },
    };

    this.gridOptions.context.eventSubject.pipe(takeUntil(this._endSubscriptions$)).subscribe(event => {
      if (event.type === 'delete') {
        this.sandbox.deletePostcode(event.data.postcode);
      }
    });

    this.sandbox.actionsSubject.pipe(ofType(distributionGroupActions.clearAssignmentInput), takeUntil(this._endSubscriptions$)).subscribe(() => {
      this.clearPostcodeInput();
    });

    this.sandbox.contactModelChangedSubject$.pipe(takeUntil(this._endSubscriptions$)).subscribe(() => {
      this.clearPostcodeInput();
    });
  }

  public assignPostcode(input: string) {
    if (input) {
      const regexp = RegExp(/^\d{5}$/);
      if (regexp.test(input)) {
        let postcode: PostcodeInterface = { postcode: input };
        this.sandbox.addPostcode(postcode);
      } else {
        this.sandbox.displayPostcodeNotNumberNotification();
      }
    } else {
      this._utilService.displayNotification('NoPostcodeSelected');
    }
  }

  public clearPostcodeInput() {
    if (this.postcodeInput) {
      this.postcodeInput.nativeElement.value = '';
    }
  }

  public ngOnDestroy(): void {
    this._endSubscriptions$.next(true);
  }
}
