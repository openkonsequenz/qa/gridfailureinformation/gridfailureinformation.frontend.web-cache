/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { LoggedOutPageComponent } from '@grid-failure-information-app/pages/logout/logged-out/logged-out.component';

describe('LoggedOutPageComponent', () => {
  let component: LoggedOutPageComponent;

  beforeEach(() => {
    component = new LoggedOutPageComponent();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should focus on button on init', () => {
    component.button = { nativeElement: { focus() {} } };
    const spy = spyOn(component.button.nativeElement, 'focus').and.callThrough();
    component.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });
});
