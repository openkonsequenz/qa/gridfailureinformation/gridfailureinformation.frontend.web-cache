/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from '@grid-failure-information-app/shared/components/components.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from '@grid-failure-information-app/shared/directives/directives.module';
import { ContainersModule } from '@grid-failure-information-app/shared/containers/containers.module';
import { LogoutPageSandbox } from '@grid-failure-information-app/pages/logout/logout/logout.sandbox';
import { LogoutPageComponent } from '@grid-failure-information-app/pages/logout/logout/logout.component';
import { LoggedOutPageComponent } from '@grid-failure-information-app/pages/logout/logged-out/logged-out.component';
import { LogoutApiClient } from '@grid-failure-information-app/pages/logout/logout-api-client';
import { EffectsModule } from '@ngrx/effects';
import { LogoutEffects } from '@grid-failure-information-app/shared/store/effects/logout/logout.effect';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    TranslateModule,
    DirectivesModule,
    RouterModule,
    FormsModule,
    ContainersModule,
    EffectsModule.forFeature([LogoutEffects]),
  ],
  declarations: [LogoutPageComponent, LoggedOutPageComponent],
  providers: [LogoutPageSandbox, LogoutApiClient],
})
export class LogoutModule {}
