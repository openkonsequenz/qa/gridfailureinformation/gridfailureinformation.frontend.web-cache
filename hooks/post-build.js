/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
var fs = require('fs-extra');
var foldersToCopy = [
  { src: './config', dest: './dist/grid-failure-information-app/config' },
  { src: './i18n', dest: './dist/grid-failure-information-app/i18n' },
];

// copies directory, even if it has subdirectories or files
function copyDir(src, dest) {
  fs.copy(src, dest, function(err) {
    if (err) return console.error(err);
    console.log(src + ' folder successfully copied');
  });
}

for (var i = foldersToCopy.length - 1; i >= 0; i--) {
  copyDir(foldersToCopy[i].src, foldersToCopy[i].dest);
}
